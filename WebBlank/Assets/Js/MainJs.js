﻿//#region Type writing

function TxtType (el, toRotate, period) {
    this.toRotate = toRotate;
    this.el = el;
    this.loopNum = 0;
    this.period = parseInt(period, 10) || 2000;
    this.txt = '';
    this.tick();
    this.isDeleting = false;
}

TxtType.prototype.tick = function () {
    var i = this.loopNum % this.toRotate.length;
    this.loopNum = i;
    var fullTxt = this.toRotate[i];

    if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
    } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
    }

    this.el.innerHTML = '<span class="Wrap">' + this.txt + '</span>';

    var that = this;
    var delta = 200 - Math.random() * 100;

    if (this.isDeleting) { delta /= 2; }

    if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
    } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
    }

    setTimeout(function () {
        that.tick();
    }, delta);
};

$(document).ready(
function ()
{
    var elements = $(".Typewriter");
    var toRotate = elements.eq(0).data("type"); // Pass all string, if uses jquery will pass in array
    var period = elements.eq(0).data("period");
    if (toRotate) {
        new TxtType(elements[0], toRotate, period);
    }
})

//#endregion

$(document).ready(
function () {

    var indi = $(".Fill");
    for (var i = 0; i < indi.length; i++) {
        var score = indi.eq(i).data("score");
        var fill = score * 0.1 * indi.eq(i).parent().width();
        
        indi.eq(i).animate({ width: fill, backgroundColor: '#FFFFFF' }, 1500);
    }   
})

